<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('Test Contact Form');
$I->amOnPage('/');
//Testcase for Contact us Form
$I->wait(1);
$I->see('career');
$I->click('career');
$I->wait(1);
// User Applying 
$I->fillField('#applicantName', 'ryan');
// Fetching correct value from the drop down
$option = $I->grabTextFrom('select option:nth-child(16)');
$I->selectOption("select", $option);
$I->wait(2);
$I->click("Apply");
// User is on Apply Now Page
$I->see('welcome');
$I->click("Apply Now");
// User is on Contact Detail Page
$I->see('Contact Detail');
$I->fillField('Email', 'ryan.sagar@ithands.net');
$I->fillField('Contact No.', '9856963256');
$I->fillField('Hometown', 'Haridwar');
$option = $I->grabTextFrom('select option:nth-child(6)'); //Year selection code
$I->selectOption("select", $option);
$I->waitForElement('//select[@id="entry_1306320130"]'); //Month selection code
$option = $I->grabTextFrom('//select[@id="entry_1306320130"]/option[@value="2"]');
$I->selectOption('//select[@id="entry_1306320130"]', $option);
$I->wait(1);
$I->fillField('Current Salary', '10000');
$I->fillField('Expected Salary', '15000');
$I->fillField('Referred By?', 'Ravi Saklani');
$I->wait(1);
// Attached File code but first you need to save file into Test/data folder...
$I->attachFile('input[name="uploadresume"]', 'sample.pdf');
$I->wait(2);
$I->click('Send');
$I->wait(2);
$I->see("Thank you for applying!");
$I->PauseExecution();
$I->wait(5);